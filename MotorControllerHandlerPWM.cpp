//-----------------------------------------------------------------------------------------
// MotorControllerHandlerPWM.cpp
//
// Controls the motor controler(s).
//
//
//----------------------------------------------------
//
// Authors:
//    Mike Schoonover 10/24/2020
//
//-----------------------------------------------------------------------------------------


#include "MotorControllerHandlerPWM.h"

//-----------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------
// class MotorControllerHandlerPWM
//
// This class controls the neck turning motor.
//

//-----------------------------------------------------------------------------------------
// MotorControllerHandlerPWM::MotorControllerHandlerPWM (constructor)
//

MotorControllerHandlerPWM::MotorControllerHandlerPWM()
{

}// end of MotorControllerHandlerPWM::MotorControllerHandlerPWM (constructor)
//-----------------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------------
// MotorControllerHandlerPWM::init
//
// top-level init that handles basic initialization and calls initSelf for specific init
// Must be called after instantiation to complete setup.
//

void MotorControllerHandlerPWM::init()
{

	setupIO();

	// if analog input pin 0 is unconnected, random analog
	// noise will cause the call to randomSeed() to generate
	// different seed numbers each time the sketch runs.
	// randomSeed() will then shuffle the random function.
	randomSeed(analogRead(0));

}// end of MotorControllerHandlerPWM::init
//-----------------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------------
// MotorControllerHandlerPWM::doRunTimeTasks
//
/**
 * This method should be called often to allow it to handle run time actions.
 *
 */

int MotorControllerHandlerPWM::doRunTimeTasks()
{

	return(0);

}// end of MotorControllerHandlerPWM::doRunTimeTasks
//-----------------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------------
// MotorControllerHandlerPWM::setupIO
//
/**
 *  Sets up generic Digital and Analog Input/Output pins. Pins for dedicated functions such
 *  as SPI, U2C, or PWM are set up in other functions.
 *
 */

void MotorControllerHandlerPWM::setupIO()
{

}// end MotorControllerHandlerPWM::setupIO
//-----------------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------------
// MotorControllerHandlerPWM::MotorControllerHandlerPWM (destructor)
//

MotorControllerHandlerPWM::~MotorControllerHandlerPWM()
{

}// end of MotorControllerHandlerPWM::MotorControllerHandlerPWM (destructor)
//-----------------------------------------------------------------------------------------

//end of class MotorControllerHandlerPWM
//-----------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------
