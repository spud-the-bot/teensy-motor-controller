//--------------------------------------------------------------------------------------------------
// InetAddress.cpp
//
//
//----------------------------------------------------
//
// Authors:
//
//	Mike Schoonover		3/13/2023
//
//
//--------------------------------------------------------------------------------------------------


#include "InetAddress.h"

//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
// class InetAddress
//
/**
 * This class handles an Internet Address and associated values. It is meant to mimic the Java
 * class InetAddress
 *
 */

//--------------------------------------------------------------------------------------------------
// InetAddress::InetAddress (constructor)
//

InetAddress::InetAddress()
{


}// end of InetAddress::InetAddress (constructor)
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
// InetAddress::InetAddress (destructor)
//

InetAddress::~InetAddress()
{

}// end of InetAddress::InetAddress (destructor)
//--------------------------------------------------------------------------------------------------

//end of class InetAddress
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
