
#ifndef _PACKET_STATUS_ENUM_H
#define _PACKET_STATUS_ENUM_H

//-----------------------------------------------------------------------------------------
// PacketStatusEnum enumeration
//
// Defines various status and error codes associated with packets.
//

enum PacketStatusEnum{

	PACKET_VALID = 0,
	UNKNOWN_PACKET_TYPE_ERROR = 1,
	DUPLEX_MATCH_ERROR = 2,
	NECK_TURN_COMPLETE = 3
};

//-----------------------------------------------------------------------------------------

#endif // _PACKET_STATUS_ENUM_H
