
#ifndef _ERRORFLAGS_H
#define _ERRORFLAGS_H

//-----------------------------------------------------------------------------------------
// ErrorFlag enumeration
//
// defines all errors that can be flagged by objects. these flags are stored in the shared
// object and later accessed by the controller for error handling.
//

enum ErrorFlag{

	NO_ERROR = 0,
	ERROR_MALLOC = 9001,
	ERROR_ADDINGCHILD = 9002,
	ERROR_ADDINGPAGE = 9003,
	ERROR_INIT_SPIFLASH = 9004,

	ERROR_CORRUPT_INI_FILE = 9005,

	ERROR_OUT_OF_BOUNDS = 9006
};

//-----------------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------------
// ExceptionFlag enumeration
//
// defines all exceptions that can be flagged by objects. these flags are stored in the shared
// object and later accessed by the controller for error handling.
//

enum ExceptionFlag{

	NO_EXCEPTION = 0

};

//-----------------------------------------------------------------------------------------

#endif // _ERRORFLAGS_H
