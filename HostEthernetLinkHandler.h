
#ifndef _HOST_ETHERNET_LINK_HANDLER_H
#define _HOST_ETHERNET_LINK_HANDLER_H

#include <Arduino.h>
#include <stdint.h>

#include "PacketTool.h"
#include "EthernetLink.h"
#include "PacketStatusEnum.h"
#include "Tools.h"

//--------------------------------------------------------------------------------------------------
// class HostEthernetLinkHandler
//
// This class handles Ethernet communication between the Control board and the Host Computer.
//

class HostEthernetLinkHandler{

	public:

		HostEthernetLinkHandler(const uint8_t pThisDeviceID,
									EthernetLink *pEthernetLink, PacketTool *const pPacketTool);

		void init();

		int doRunTimeTasks();

		void setupIO();

		EthernetLink *getEthernetLink(){ return(ethernetLink); }

		PacketTool *getPacketTool(){ return(packetTool); }

		void sendACKPkt(PacketTypeEnum pSourcePktType, PacketStatusEnum pStatus);

		virtual ~HostEthernetLinkHandler();

	protected:

		int handleEthernetConnection();

		int handleCommunications();

		int handlePacket();

	private:

	// class member variables

	public:

	protected:

		uint8_t thisDeviceID;

		EthernetLink *ethernetLink;

		PacketTool *packetTool;

	private:


};// end of class HostEthernetLinkHandler
//--------------------------------------------------------------------------------------------------

#endif // _HOST_ETHERNET_LINK_HANDLER_H