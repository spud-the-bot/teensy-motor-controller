/******************************************************************************
* Title: EthernetLink.java
* Author: Mike Schoonover
* Date: 07/12/2020
*
* Purpose:
*
* This class handles the link to a remote device via Ethernet connections.
*
* Open Source Policy:
*
* This source code is Public Domain and free to any interested party.  Any
* person, company, or organization may do with it as they please.
*
*/

//-----------------------------------------------------------------------------

/*
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import controller.LoggerBasic;

import java.net.InetSocketAddress;
import java.net.SocketAddress;

 */

#include "EthernetLink.h"

//-----------------------------------------------------------------------------
// class EthernetLink
//
//
//

	// Enter a MAC address and IP address for your controller below.
	// The IP address will be dependent on your local network.
	// gateway and subnet are optional:

	// The Teensy Motor Controllers use: 42:42:42:42:42:??
	// where ?? is selected by DIP switches on the carrier board

	byte mac[] = {
	  0x42, 0x42, 0x42, 0x42, 0x42, 0x01
	};

	IPAddress ip(192, 168, 1, 177);
	IPAddress myDns(42, 42, 42, 1);
	IPAddress gateway(42, 42, 42, 1);
	IPAddress subnet(255, 255, 255, 0);

	const int LINE_TERMINATOR = 0x0a;

	const int READ_BYTES_LIMIT = 10000;

	const int NUM_BYTES_TO_READ_FOR_CLEARING = 100000;

	const int SOCKET_CONNECT_TIMEOUT = 5000;

	const int DEVICE_RESPONSE_TIMEOUT = 5000;

	const int READ_LOOP_TIMEOUT = 50;

	const int READ_SLEEP_MS = 10;

	const int PORT_TIMEOUT = 50;


//-----------------------------------------------------------------------------
// EthernetLink::EthernetLink (constructor)
//

EthernetLink::EthernetLink(EthernetServer *pServer, int pThisDevicePort):
  thisDevicePort(pThisDevicePort), isDHCPConfigured(false), server(pServer), client(nullptr),
  isConnected(false)
{

}//end of EthernetLink::EthernetLink (constructor)
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// EthernetLink::init
//
// Initializes the object.  Must be called immediately after instantiation.
//

void EthernetLink::init(LoggerBasicInterface *pThreadSafeLogger)
{

    tsLog = pThreadSafeLogger; numBytesRead = 0;

}// end of EthernetLink::init
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// EthernetLink::getStream
//
// Returns a Stream pointer to the EthernetClient object. Class EthernetClient extends Stream,
// so it can be cast as such.
//

Stream* EthernetLink::getStream()
{

	Serial.print("client stream pointer in getStream: "); //debug mks
	Serial.println((uint32_t)client); //debug mks

    return((Stream*) client);

}// end of EthernetLink::init
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// EthernetLink::configureDHCPByRouter
//
/**
 * Retrieves the DHCP information from the router if possible.
 *
 * Returns 1 if successful, 0 if not successful, -1 on error.
 *
 */

int EthernetLink::configureDHCPByRouter()
{

	if(getIsDHCPConfigured()){ return(1); }

	if (Ethernet.begin(mac) == 0){ return 0; }

	isDHCPConfigured = true;

	Serial.print("This device IP address: "); Serial.println(Ethernet.localIP());

	server->begin(); // start listening for clients

	return(1);

}// end of EthernetLink::configureDHCPByRouter
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// EthernetLink::connectToRemote
//
/**
 * Establishes a connection with the remote device if possible.
 *
 * The EthernetServer.available function returns an EthernetClient object which must be stored as
 * a persistent object in class member variable clientObject. A pointer to that object is stored
 * in class member variable client for all other usage.
 *
 * @param pIPAddr			the IP address of the remote device
 * @param pPort				the port on the remote device
 * @return					0 on no connection made, 1 on new connection made, -1 on error
 *
 */

int EthernetLink::connectToRemote()
{

	Serial.println("Attempting to connect to remote..."); //debug mks

	clientObject = server->available(); // check for a new client request

	// when the client sends the first byte, say hello:
	if (clientObject) {
		Serial.println("We have a new client!");
		isConnected = true;
		client = &clientObject;
		return(1);
	}

	return(0);

//    ipAddr = pIPAddr; ipAddrS = ipAddr.toString();

// wip mks
//    if (ipAddrS == null || ipAddr == null){
//        tsLog.logMessage("Error 119: IP Address is null.\n");
//        return(-1);
//    }

/*

    try {

        tsLog.logMessage("Connecting to device " + ipAddrS + "...\n");

		socket = new Socket();
		socket.setReuseAddress(true);
		SocketAddress endPoint = new InetSocketAddress(ipAddr, pPort);

        if (!simulate) {
			socket.connect(endPoint, SOCKET_CONNECT_TIMEOUT);
		} else {
            //UTSimulator utSimulator = new UTSimulator(indexNum, simChassisNum,
            //  simSlotNum, backplaneSimulator, ipAddr, 23, mainFileFormat,
            //  simulationDataSourceFilePath, getMaxNumberOfClockPositions());
            //utSimulator.init();

            //socket = utSimulator;
        }

        //set amount of time in milliseconds that a read from the socket will
        //wait for data - this prevents program lock up when no data is ready
        socket.setSoTimeout(PORT_TIMEOUT);

        // the buffer size is not changed here as the default ends up being
        // large enough - use this code if it needs to be increased
        //socket.setReceiveBufferSize(10240 or as needed);

        //allow verification that the hinted size is actually used
        tsLog.logMessage("Device " + ipAddrS + " receive buffer size: " +
                                      socket.getReceiveBufferSize() + "...\n");

        //allow verification that the hinted size is actually used
        tsLog.logMessage("Device " + ipAddrS + " send buffer size: " +
                                        socket.getSendBufferSize() + "...\n");

        in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

        byteOut = new DataOutputStream(socket.getOutputStream());
        byteIn = new DataInputStream(socket.getInputStream());

    }
    catch (UnknownHostException e) {
        logSevere(e.getMessage() + " - Error: 160");
        tsLog.logMessage("Unknown host: Device " + ipAddrS + ".\n");
        return(-1);
    }
    catch (IOException e) {
        logSevere(e.getMessage() + " - Error: 165");
        tsLog.logMessage("Couldn't get I/O for Device " + ipAddrS + "\n");
        tsLog.logMessage("--" + e.getMessage() + "--\n");
        return(-1);
    }


//    try {
//        //display the greeting message sent by the remote
//        tsLog.logMessage("Device " + ipAddrS + " says " + in.readLine() + "\n");
//    }
//    catch(IOException e){
//        logSevere(e.getMessage() + " - Error: 748");
//    }
*/

	//tsLog->logMessage("Device " + ipAddrS + " is connected.\n");

}// end of EthernetLink::connectToRemote
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// EthernetLink::connectToRemote
//
/**
 * Connects to remote device using a com port. Not used in this class.
 *
 *
 * @param pSelectedCOMPort	the name of the Serial COM port to be used for the connection
 * @param pBaudRate			baud rate for the connection
 * @return					always returns -1 which designates failure
 *
 */

int EthernetLink::connectToRemote(std::string pSelectedCOMPort, int pBaudRate)
{

	return(-1);

}// end of EthernetLink::connectToRemote
//-----------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
// EthernetLink::doRunTimeTasks
//
/**
 * Handles runtime tasks..
 *
 * This function should be called often during run time to allow for continuous processing.
 *
 */

int EthernetLink::doRunTimeTasks()
{

	return(0);

}// end of EthernetLink::doRunTimeTasks
//--------------------------------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// EthernetLink::closeConnectionToRemote
//
/**
 * Closes the com port connection. Also sets the DTR line low.
 *
 * @return	true if port successfully closed, false if not
 *
 */

bool EthernetLink::closeConnectionToRemote()
{

	isDHCPConfigured = false;
	isConnected = false;

/*

    try{

        if (byteOut != null) {byteOut.close();}
        if (byteIn != null) {byteIn.close();}
        if (in != null) {in.close();}
        if (socket != null) {socket.close();}

    }
    catch(IOException e){
        logSevere(e.getMessage() + " - Error: 261");
		return(false);
    }

 *
 */

	return(true);

}// end of EthernetLink::closeConnectionToRemote
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// EthernetLink::disposeOfAllResources
//
// Releases all objects created which may have back references. This is
// necessary because some of those objects may hold pointers back to a parent
// object (such as Java Listeners and such). These back references will prevent
// the parent object from being released and a memory leak will occur.
//
// Not all objects must be explicitly released here as most will be
// automatically freed when this object is released.
//

void EthernetLink::disposeOfAllResources()
{


}//end of EthernetLink::disposeOfAllResources
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// EthernetLink::getInputStream
//
/**
 * Returns an InputStream for the serial port.
 *
 * @return InputStream for the serial port.
 *
*/

EthernetClient* EthernetLink::getInputStream()
{

	return(nullptr);

}// end of EthernetLink::getInputStream
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// EthernetLink::getOutputStream
//
/**
 * Returns an OutputStream for the serial port.
 *
 * @return OutputStream for the serial port.
 *
*/

EthernetClient* EthernetLink::getOutputStream()
{

	return(nullptr);

}// end of EthernetLink::getOutputStream
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// EthernetLink::sendString
//
// Sends a String to the remote device. A LINE_TERMINATOR value is sent as the last byte, so it
// should not be added to the String.
//

void EthernetLink::sendString(std::string pString)
{

 /*

	if(byteOut == null){ return; }

	try {

		byteOut.writeBytes(pString + LINE_TERMINATOR);

	} catch (IOException e) {
		logSevere(e.getMessage() + " - Error: 332");
	}

  */

}// end of EthernetLink::sendString
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// EthernetLink::readStringResponse
//
// Calls readStringResponse(READ_BYTES_LIMIT).
//
// See readStringResponse(int pMaxNumBytes) for details.
//

std::string EthernetLink::readStringResponse()
{

	return(readStringResponse(READ_BYTES_LIMIT));

}// end of EthernetLink::readStringResponse
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// EthernetLink::readStringResponse
//
/**
 * Reads a character response from the remote device. Characters will be read until pMaxNumBytes
 * have been read, LINE_TERMINATOR is received to indicate the end of the response, or timeout
 * occurs. The response will be returned as a String. If timeout before any characters received, the
 * String will have length of 0. If timeout before LINE_TERMINATOR is received, the last character
 * of the String will not be LINE_TERMINATOR.
 *
 * timeout in milliseconds ~= READ_LOOP_TIMEOUT * READ_SLEEP_MS
 *
 * NOTE: this function is not normally used when data packets are used to communicate. It might
 * be used to receive an initial greeting, but that could also be done using packets.
 *
 * @param pMaxNumBytes	the maximum number of bytes to be read
 * @return				the string read - terminated if terminator received before timeout or
 *						pMaxNumBytes characters read; empty string if timeout occurs before any
 *						characters received; null if an error occurs
 *
 */

std::string EthernetLink::readStringResponse(int pMaxNumBytes)
{

/*

	if(byteIn == null){ return(""); }

	int timeoutLoopCounter = 0;
	boolean done = false;
	byte[] inByte = new byte[1];

	inString.setLength(0);

	try {


		while (!done && timeoutLoopCounter++ < READ_LOOP_TIMEOUT){

			while(!done && byteIn.available() != 0){

				byteIn.read(inByte, 0, 1);

				inString.append((char)inByte[0]);

				timeoutLoopCounter = 0;

				if(inString.length() == pMaxNumBytes){ done = true; break; }

				if(inByte[0] == LINE_TERMINATOR){ done = true; }
			}

			if(!done) { threadSleep(READ_SLEEP_MS); }

		}
	}
	catch(IOException e){
        logSevere(e.getMessage() + " - Error: 411");
		return(null);
	}

	return(inString.toString());

 */

	return("WIP");

}// end of EthernetLink::readStringResponse
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// EthernetLink::clearReceiveBuffer
//
/**
 * Clears the receive buffer by reading NUM_BYTES_TO_READ_FOR_CLEARING number of bytes or until a
 * timeout occurs.
 *
 * @return number of bytes cleared, -1 on error
 *
 */

int EthernetLink::clearReceiveBuffer()
{

	/*

	try{
		if(byteIn.available() <= 0){ return(0); }

	}
	catch(IOException e){
        logSevere(e.getMessage() + " - Error: 411");
		return(-1);
	}

	readBytesResponse(NUM_BYTES_TO_READ_FOR_CLEARING);

	return(numBytesRead);
	 *
	 */

	return(0);

}// end of EthernetLink::clearReceiveBuffer
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// EthernetLink::readBytesResponse
//
/**
 * Reads a byte block response from the remote device. Characters will be read until pMaxNumBytes
 * have been read or timeout occurs.
 *
 * The response will be returned as a byte array. If timeout before any characters received, the
 * byte array will be empty.
 *
 * The number of bytes read will be stored in class member variable numBytesRead.
 *
 * timeout in milliseconds ~= READ_LOOP_TIMEOUT * READ_SLEEP_MS
 *
 * @param pMaxNumBytes	the maximum number of bytes to be read
 * @return				byte array containing bytes read from stream; may be empty
 *						the number of bytes read is stored in class member variable numBytesRead
 *
 */

char* EthernetLink::readBytesResponse(int pMaxNumBytes)
{

/*

	if(byteIn == null){ return(null); }

	int timeoutLoopCounter = 0;
	boolean done = false;
	byte[] inByte = new byte[1];
	byte[] inBytes = new byte[pMaxNumBytes];

	int i = 0;

	try {

		while (!done && timeoutLoopCounter++ < READ_LOOP_TIMEOUT){

			while(!done && byteIn.available() != 0){

				byteIn.read(inByte, 0, 1);

				inBytes[i++] = inByte[0];

				timeoutLoopCounter = 0;

				if(i == pMaxNumBytes){ done = true; break; }

			}

			if(!done) { threadSleep(READ_SLEEP_MS); }

		}
	}
	catch(IOException e){
        logSevere(e.getMessage() + " - Error: 502");
		return(null);
	}

	numBytesRead = i;

	return(inBytes);
 *
 */

	return(nullptr);

}// end of EthernetLink::readBytesResponse
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// EthernetLink::convertBytesToCommaDelimitedDecimalString
//
// Returns the bytes in pBuffer as a comma-delimited string of decimal numbers.
//

std::string EthernetLink::convertBytesToCommaDelimitedDecimalString(char *pBuffer)
{

	/*

	inString.setLength(0);

	for(byte b : pBuffer){
		inString.append(b);
		inString.append(',');
	}

	return(inString.toString());
	 *
	 */

	return("stub function");

}// end of EthernetLink::convertBytesToCommaDelimitedDecimalString
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// EthernetLink::getIsDHCPConfigured
//

bool EthernetLink::getIsDHCPConfigured()
{

	return(isDHCPConfigured);

}//end of EthernetLink::getIsDHCPConfigured
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// EthernetLink::getIsConnected
//

bool EthernetLink::getIsConnected()
{

	return(isConnected);

}//end of EthernetLink::getIsConnected
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// EthernetLink::threadSleep
//
// Calls the Thread.sleep function. Placed in a function to avoid the
// "Thread.sleep called in a loop" warning -- yeah, it's cheezy.
//
// Returns true if InterruptedException caught...this usually means some other thread wants to
// shut down this thread.
//

bool EthernetLink::threadSleep(int pSleepTime)
{

    //Thread.sleep(pSleepTime);

	return(false);

}//end of EthernetLink::threadSleep
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// EthernetLink::~EthernetLink (destructor)
//
/**
 * Destructor.
 *
 */

EthernetLink::~EthernetLink()
{

}//end of EthernetLink::~EthernetLink (destructor)
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// EthernetLink::logSevere
//
// Logs pMessage with level SEVERE using the Java logger.
//

void EthernetLink::logSevere(std::string pMessage)
{

    //Logger.getLogger(getClass().getName()).log(Level.SEVERE, pMessage);

}//end of EthernetLink::logSevere
//-----------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
// Functions not yet implemented but must be included as they are in an interface.
//

	void EthernetLink::open() {
		//throw new UnsupportedOperationException("Not supported yet.");
	}

	void close() {
		//throw new UnsupportedOperationException("Not supported yet.");
	}

	char readByte() {
		return('0');
		//throw new UnsupportedOperationException("Not supported yet.");
	}

	char* readBytes(int pNumBytes) {
		return(nullptr);
		//throw new UnsupportedOperationException("Not supported yet.");
	}

	char peek() {
		return('0');
		//throw new UnsupportedOperationException("Not supported yet.");
	}

	int readBytesResponse(long pMaxNumBytes, char *pByteArray, IntParameter pNumBytesRead){
		return(0);
		//throw new UnsupportedOperationException("Not supported yet.");
	}

	int sendBytes(char* pBytes, int pNumBytes){
		return(0);
		//throw new UnsupportedOperationException("Not supported yet.");
	}

	int waitForData(long pNumBytesToWaitFor, long pTimeOut, IntParameter pNumBytesAvailable){
		return(0);
		//throw new UnsupportedOperationException("Not supported yet.");
	}

	bool setDTR(){
		return(false);
		//throw new UnsupportedOperationException("Not supported yet.");
	}

	bool clearDTR(){
		return(false);
		//throw new UnsupportedOperationException("Not supported yet.");
	}

//end of functions not yet implemented
//-----------------------------------------------------------------------------


//end of class EthernetLink
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
