
#ifndef _LOGGER_BASIC_INTERFACE_H
#define _LOGGER_BASIC_INTERFACE_H

#include <Arduino.h>
#include <vector>
#include <string>

//--------------------------------------------------------------------------------------------------
// class LoggerBasicInterface
//
// This class is an interface for basic logging functions.
//

class LoggerBasicInterface{

	public:

		virtual void logMessage(std::string pMessage);

		virtual void logMessageThreadSafe();

		virtual void separate();

		virtual void date();

		virtual void section();

		virtual void saveToFile(std::string pFilenameSuffix);

		virtual void saveToFileThreadSafe();

		virtual void displayErrorMessage(std::string pMessage);

		virtual void appendString(std::string pText);

		virtual void appendArrayListOfStrings(std::vector<std::string*> pStrings);

		virtual void appendLine(std::string pText);

		virtual void appendToErrorLogFile(std::string pMessage);

};// end of class LoggerBasicInterface
//--------------------------------------------------------------------------------------------------

#endif // _LOGGER_BASIC_INTERFACE_H