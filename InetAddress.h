
#ifndef _INET_ADDRESS_H
#define _INET_ADDRESS_H

#include <Arduino.h>
#include <string>
//#include <stdint>

//--------------------------------------------------------------------------------------------------
// class InetAddress
//
// This class handles an Internet Address and associated values. It is meant to mimic the
// Java class InetAddress.
//

class InetAddress{

	public:

		InetAddress();

		std::string toString(); //wip mks -- return the string

		virtual ~InetAddress();

	protected:


	protected:

	public:


};// end of class InetAddress
//--------------------------------------------------------------------------------------------------

#endif // _INET_ADDRESS_H